import Dashboard from "../icons/Dashboard.svg";
import Office from "../icons/Office.svg";
import Enquiries from "../icons/Enquiries.svg";
import Clients from "../icons/Client.svg";
import Services from "../icons/Service.svg";
import Quotation from "../icons/Quotation.svg";
import Tasks from "../icons/Tasks.svg";
import ThreeDot from "../icons/ThreeDot.svg";
export const SidebarData = [
  {
    id: 1,
    title: "Dashboard",
    icon: Dashboard,
    icon_sub: ThreeDot,
  },
  {
    id: 2,
    title: "Office Check-in",
    icon: Office,
    icon_sub: ThreeDot,
  },
  {
    id: 3,
    title: "Enquiries",
    icon: Enquiries,
    icon_sub: ThreeDot,
  },
  {
    id: 4,
    title: "Clients",
    icon: Clients,
    icon_sub: ThreeDot,
  },
  {
    id: 5,
    title: "Services",
    icon: Services,
    icon_sub: ThreeDot,
  },
  {
    id: 6,
    title: "Quotation",
    icon: Quotation,
    icon_sub: ThreeDot,
  },
  {
    id: 7,
    title: "Tasks",
    icon: Tasks,
    icon_sub: ThreeDot,
  },
];
