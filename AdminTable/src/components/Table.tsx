import React, { useState } from "react";
import DualArrow from "../assets/icons/DualArrow.svg";
import ThreeDot from "../assets/icons/ThreeDot.svg";
import Plus from "../assets/icons/Plus.svg";
import UserImage from "../assets/icons/Userimg.svg";
import ToggleButton from "./ToggleButton";
import CheckBox from "./Checkbox";

const initialRows = [
  {
    Name: "Nisha Giri Puri",
    Addedfrom: "Jon",
    Tags: "-",
    InternalId: 123,
    ClientId: "-",
    PhoneId: 789,
    ClientPortal: "Portal1",
    Assignee: "Assignee1",
  },
  {
    Name: "Abram Press",
    Addedfrom: "Jane",
    Tags: "-",
    InternalId: 124,
    ClientId: "-",
    PhoneId: 790,
    ClientPortal: "Portal2",
    Assignee: "Assignee2",
  },
];

const columns = [
  { field: "Name", headerName: "Name" },
  { field: "Addedfrom", headerName: "Title" },
  { field: "Tags", headerName: "Type" },
  { field: "InternalId", headerName: "Internal Id" },
  { field: "ClientId", headerName: "Client Id" },
  { field: "PhoneId", headerName: "Phone Id" },
  { field: "ClientPortal", headerName: "Client Portal" },
  { field: "Assignee", headerName: "Assignee" },
];

const columnWidth = 800;

export default function Table() {
  const [rows, setRows] = useState<any>(initialRows);
  const [editedRow, setEditedRow] = useState<any>(null);
  const [hiddenColumns, setHiddenColumns] = useState<any>([]);
  const [showAddColumnModal, setShowAddColumnModal] = useState<any>(false);

  const [newClientData, setNewClientData] = useState({
    Name: "",
    Addedfrom: "",
    Tags: "",
    InternalId: "",
    ClientId: "",
    PhoneId: "",
    ClientPortal: "",
    Assignee: "",
  });

  const handleInputChange = (field: any, value: any, index: number) => {
    if (index === editedRow) {
      setRows((prevRows: any) => {
        const updatedRows = [...prevRows];
        updatedRows[index] = { ...updatedRows[index], [field]: value };
        return updatedRows;
      });
    } else {
      setEditedRow(index);
    }
  };

  const handleAddClientDetails = () => {
    const newClient = {
      ...newClientData,
    };

    setRows((prevRows) => [...prevRows, newClient]);
    setNewClientData({
      Name: "",
      Addedfrom: "",
      Tags: "",
      InternalId: "",
      ClientId: "",
      PhoneId: "",
      ClientPortal: "",
      Assignee: "",
    });
  };

  const toggleColumnVisibility = (field: any) => {
    console.log(field, "field")
    setHiddenColumns([...hiddenColumns, field]);
  };

  const showColumn = (field: any) => {
    setHiddenColumns(hiddenColumns.filter((col: string) => col !== field));
  };

  const handleAddColumnModal = () => {
    setShowAddColumnModal(true);
  };

  const handleHideColumn = (field: any) => {
    toggleColumnVisibility(field);
  };

  return (
    <div className="container h-[66vh] w-full overflow-x-auto">
      <div className="flex flex-col">
        <div className="container">
          <div className="p-1.5 w-full inline-block align-middle">
            <div className="overflow-hidden border">
              <table className="min-w-full divide-y divide-gray-200">
                <colgroup>
                  <col style={{ width: "100px" }} />
                  {columns?.map((details, index) => (
                    <col key={index} style={{ width: `${columnWidth}px` }} />
                  ))}
                  <col style={{ width: "200px" }} />
                </colgroup>
                <thead className="">
                  <tr>
                    <th
                      scope="col"
                      className="py-3 pl-3 pr-1 border-r border-gray-200 w-[100px]"
                    >
                      <div className="flex items-center h-5">
                        <CheckBox />
                      </div>
                    </th>
                    {columns?.map((details, index) => (
                      <th
                        key={index}
                        scope="col"
                        className={`px-6 py-3 text-[12px] text-[#344054] font-medium text-left border-r w-[600px] border-gray-200 ${hiddenColumns.includes(details.field) ? "hidden" : ""
                          }`}
                      >
                        <div className="flex items-center justify-between w-full">
                          <div className="flex gap-2 items-center">
                            {details?.headerName}{" "}
                            <img
                              src={DualArrow}
                              alt="arrow"
                              className="h-[14px] w-[14px] cursor-pointer"
                            />
                          </div>
                          <img
                            src={ThreeDot}
                            alt="three dot"
                            className="cursor-pointer"
                            onClick={() =>
                              toggleColumnVisibility(details.field)
                            }
                          />
                        </div>
                      </th>
                    ))}
                    <th
                      scope="col"
                      className={`px-6 py-3 text-[12px] text-[#344054] font-medium text-left border-r border-gray-200 cursor-pointer ${hiddenColumns.length === columns.length ? "hidden" : ""
                        }`}
                      onClick={handleAddColumnModal}
                    >
                      <div className="flex items-center justify-between">
                        <div className="flex gap-2 items-center">
                          Add Column{" "}
                          <img
                            src={Plus}
                            alt="plus"
                            className="h-[14px] w-[14px] cursor-pointer"
                          />
                        </div>
                      </div>
                    </th>
                  </tr>
                </thead>
                <tbody className="divide-y divide-gray-200">
                  {rows?.map((details, index) => (
                    <tr key={index} className="bg-white ">
                      <td className="py-3 pl-3 border-r border-gray-200">
                        <div className="flex items-center h-5">
                          <CheckBox />
                        </div>
                      </td>
                      {columns.map((column, colIndex) => (
                        <td
                          key={colIndex}
                          className={`px-6 py-4 text-sm font-medium text-gray-800 whitespace-nowrap border-r border-gray-200 ${hiddenColumns.includes(column.field) ? "hidden" : ""
                            }`}
                        >
                          <div className="flex items-center gap-2">
                            {column.field === "Name" ? (
                              <div className="flex items-center gap-2">
                                <img src={UserImage} alt="user" />
                                <div className="flex flex-col">
                                  <input
                                    type="text"
                                    value={details.Name}
                                    placeholder="Name"
                                    onChange={(e) =>
                                      handleInputChange(
                                        "Name",
                                        e.target.value,
                                        index
                                      )
                                    }
                                    className="w-full focus:outline-none bg-transparent"
                                  />
                                  <p className="text-[12px] font-normal leading-[17px] text-[#667085]">
                                    test@gmail.com
                                  </p>
                                </div>
                              </div>
                            ) : (
                              <input
                                type="text"
                                value={details[column.field]}
                                placeholder={column.headerName}
                                className="w-full focus:outline-none bg-transparent"
                                onChange={(e) =>
                                  handleInputChange(
                                    column.field,
                                    e.target.value,
                                    index
                                  )
                                }
                              />
                            )}
                          </div>
                        </td>
                      ))}
                    </tr>
                  ))}
                  <tr className="bg-white">
                    <td className="py-3 pl-3 border-r border-gray-200">
                      <div className="flex items-center h-5">
                        <CheckBox />
                      </div>
                    </td>
                    {columns.map((column, colIndex) => (
                      <td
                        key={colIndex}
                        className={`px-6 py-4 text-sm font-medium text-gray-800 whitespace-nowrap border-r border-gray-200 ${hiddenColumns.includes(column.field) &&
                          column.field !== "Name"
                          ? "hidden"
                          : ""
                          }`}
                      >
                        <div className="flex items-center gap-2">
                          {column.field === "Name" && (
                            <div
                              className="flex items-center gap-2"
                              onClick={handleAddClientDetails}
                            >
                              <img
                                src={Plus}
                                alt="plus"
                                className="w-[14px] h-[14px]"
                              />
                              <button className="w-full focus:outline-none bg-transparent">
                                Add Client's details
                              </button>
                            </div>
                          )}

                        </div>
                      </td>
                    ))}
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      {showAddColumnModal && (
        <div className="fixed inset-0 bg-black bg-opacity-50 z-50 flex justify-center items-center">
          <div className="bg-white p-4 rounded-md w-[500px]">
            <p className="text-lg font-semibold mb-2">Hidden Columns</p>
            <ul>
              {columns.map((column) => (
                <li key={column.field} className="mb-2">
                  {hiddenColumns.includes(column.field) ? (
                    <div className="flex items-center gap-5">
                      <button className="bg-[#22c55e] px-2 py-1 rounded-md text-[white] cursor-pointer" onClick={() => showColumn(column.field)}>
                        Show
                      </button>
                      <p> {column.headerName}</p>
                    </div>
                  ) : (
                    <div className="flex items-center gap-5">
                      <button className="bg-[red] px-2 py-1 rounded-md text-[white] cursor-pointer" onClick={() => handleHideColumn(column.field)}>
                        Hide
                      </button>
                      <p>{column.headerName}</p>
                    </div>
                  )}
                </li>
              ))}
            </ul>
            <button
              className="bg-[red] text-white p-2 rounded-md "
              onClick={() => setShowAddColumnModal(false)}
            >
              Close
            </button>
          </div>
        </div>
      )}
    </div>
  );
}
