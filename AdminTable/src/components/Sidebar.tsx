import React, { useState } from "react";
import { SidebarData } from "../assets/data/Sidebar";
import LeftArrow from "../assets/icons/LeftArrow.svg";
import DownArrow from "../assets/icons/DownArrow.svg";
import Search from "../assets/icons/Search.svg";
import ThreeDot from "../assets/icons/ThreeDot.svg";
const Sidebar = () => {
  const [active, setActive] = useState(SidebarData[0]?.title);
  return (
    <div className="bg-[white] h-full">
      <div className="pl-[24px] py-[12px] relative">
        <div className="flex items-center gap-2">
          <p className="text-[16px] font-medium leading-[22px] text-[#475467]">
            CRM
          </p>
          <img src={DownArrow} alt="down" />
        </div>
        <img src={LeftArrow} alt="left" className="absolute right-0 top-0" />
      </div>
      <div className="flex flex-col px-[24px] pt-[8px] pb-[24px] gap-[24px]">
        <div className="flex items-center justify-between px-[12px] py-[8px] border rounded-[4px]">
          <div className="flex items-center gap-[12px]">
            <img src={Search} alt="threeDot" />
            <input
              type="text"
              placeholder="Search"
              className="focus:outline-none text-[14px] leading-[20px]"
            />
          </div>
          <img src={ThreeDot} alt="threedot" />
        </div>
        <div className="flex flex-col items-center w-full">
          {SidebarData?.map((details: any) => (
            <div
              onClick={() => setActive(details?.title)}
              key={details?.id}
              className={`flex items-center justify-between w-full px-[12px] py-[8px] cursor-pointer ${
                active === details?.title ? "bg-[#ebebf8] rounded-md" : ""
              }`}
            >
              <div className="flex gap-[12px]">
                <img src={details?.icon} alt={details?.title} />
                <p className="text-[12px] text-[#475467] font-medium leading-[17px]">
                  {details?.title}
                </p>
              </div>
              <img src={details?.icon_sub} alt="subIcon" />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
