import React, { useState } from 'react';

const CheckBox = () => {
  const [isChecked, setIsChecked] = useState(false);

  const toggleCheckbox = () => {
    setIsChecked(!isChecked);
  };

  return (
    <label className="checkbox-container">
      <input
        type="checkbox"
        checked={isChecked}
        onChange={toggleCheckbox}
        id="checkbox-all"
      />
      <div className={`custom-checkbox ${isChecked ? 'checked' : ''}`}>
        {isChecked && <span>&#10003;</span>}
      </div>
    </label>
  );
};

export default CheckBox;
