import React from "react";
import User from "../assets/icons/User.svg";
import DownArrow from "../assets/icons/DownArrow.svg";
import ThreeDot from "../assets/icons/ThreeDot.svg";
import Dropdown from "./Dropdown";
const MainTableHeader = () => {
  const people = [
    { id: 1, name: "Durward Reynolds", unavailable: false },
    { id: 2, name: "Kenton Towne", unavailable: false },
    { id: 3, name: "Therese Wunsch", unavailable: false },
    { id: 4, name: "Benedict Kessler", unavailable: true },
    { id: 5, name: "Katelyn Rohan", unavailable: false },
  ];
  return (
    <div className="flex items-center justify-between px-[24px] h-[60px] bg-white ">
      <div className="flex gap-[12px]">
        <img src={User} alt="user" />
        <p className="text-[16px] font-medium leading-[22px] text-[#344054]">
          Clients
        </p>
      </div>
      <div className="flex items-center gap-[24px]">
        <Dropdown people={people}/>
        <img src={ThreeDot} alt="three dot" />
      </div>
    </div>
  );
};

export default MainTableHeader;
