import React from "react";
import Menu from "../assets/icons/Menu.svg";
import Plus from "../assets/icons/Plus.svg";
import Bell from "../assets/icons/Bell.svg";
import Mail from "../assets/icons/Mail.svg";
import Settings from "../assets/icons/Settings.svg";
import Line from "../assets/icons/Line.svg";
import Moon from "../assets/icons/Moon.svg";
import DownArrow from "../assets/icons/DownArrow.svg";
import Avatar from "../assets/icons/Avatar.svg";
const Navbar = () => {
  return (
    <div className="px-[30px] h-[72px] flex items-center justify-between bg-white">
      <div className="flex items-center gap-4">
        <img src={Menu} alt="menu" />
        <p className="text-[20px] font-semibold leading-7">Test Project</p>
      </div>
      <div className="flex items-center gap-6">
        <img src={Plus} alt="plus" className="cursor-pointer" />
        <div className="relative cursor-pointer">
          <img src={Bell} alt="bell" />
          <p className="absolute -top-1 right-0 text-[10px] font-medium leading-3 rounded-full bg-green-500 text-white h-[14px] w-[14px] text-center">
            3
          </p>
        </div>
        <img src={Mail} alt="plus" className="cursor-pointer" />
        <img src={Settings} alt="plus" className="cursor-pointer" />
        <img src={Line} alt="plus" />
        <img src={Moon} alt="moon" className="cursor-pointer" />
        <div className="flex items-center gap-1 cursor-pointer">
          <img src={Avatar} alt="avatar" />
          <img src={DownArrow} alt="downarrow"/>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
