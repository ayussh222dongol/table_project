import { useState, Fragment } from "react";
import { Listbox, Transition } from "@headlessui/react";
import DownArrow from "../assets/icons/DownArrow.svg";


function Dropdown({ icon,people }: any) {
  const [selectedPerson, setSelectedPerson] = useState(people[0]);

  return (
    <div>
      <Listbox value={selectedPerson} onChange={setSelectedPerson}>
        <Listbox.Button className="border rounded-[4px] p-[8px] flex items-center text-[14px] leading-[16px] text-[#667085] gap-[6px]">
          {icon ? <img src={icon} alt="icon"/> : null}
          {selectedPerson.name}
          <img src={DownArrow} alt="down arrow" />
        </Listbox.Button>
        <Transition
          as={Fragment}
          leave="transition ease-in duration-100"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <Listbox.Options className="absolute mt-1 max-h-60 overflow-auto cursor-pointer flex flex-col gap-2 p-3 rounded-md bg-white py-1 text-base  focus:outline-none ">
            {people.map((person) => (
              <Listbox.Option
                key={person.id}
                value={person}
                disabled={person.unavailable}
              >
                {person.name}
              </Listbox.Option>
            ))}
          </Listbox.Options>
        </Transition>
      </Listbox>
    </div>
  );
}

export default Dropdown;
