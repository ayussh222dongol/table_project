import React, { useEffect, useState } from "react";

const ToggleButton = ({ showColumn, handleHideColumn, column }: any) => {
  const [isChecked, setIsChecked] = useState(false);

  const handleToggle = (e: any) => {
    e.stopPropagation();
    setIsChecked((prev) => !prev);
    if (!isChecked) {
      handleHideColumn(column);
    } else {
      showColumn(column);
    }
  };

  return (
    <label className="relative inline-block w-12 h-6 cursor-pointer">
      <input
        type="checkbox"
        className="sr-only"
        checked={isChecked}
        onChange={handleToggle}
      />
      <div
        className={`w-12 h-6 transition-all duration-300 ease-in-out ${
          isChecked ? "bg-green-500" : "bg-gray-300"
        } rounded-full flex items-center px-1`}
      >
        <div
          className={`w-4 h-4 bg-white rounded-full shadow-md transform ${
            isChecked ? "translate-x-6" : "translate-x-0"
          }`}
        ></div>
      </div>
    </label>
  );
};

export default ToggleButton;
