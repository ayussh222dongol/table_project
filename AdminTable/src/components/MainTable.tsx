import React, { useState } from "react";
import MainTableHeader from "./MainTableHeader";
import Search from "../assets/icons/Search.svg";
import Dropdown from "./Dropdown";
import Filter from "../assets/icons/Filter.svg";
import Calander from "../assets/icons/Calander.svg";
import Tick from "../assets/icons/Tick.svg";
import Line from "../assets/icons/Line.svg";
import DualArrow from "../assets/icons/DualArrow.svg";
import WhiteArrow from "../assets/icons/WhiteArrow.svg";
import Table from "./Table";
const MainTable = () => {
  const people = [
    { id: 1, name: "Durward Reynolds", unavailable: false },
    { id: 2, name: "Kenton Towne", unavailable: false },
    { id: 3, name: "Therese Wunsch", unavailable: false },
    { id: 4, name: "Benedict Kessler", unavailable: true },
    { id: 5, name: "Katelyn Rohan", unavailable: false },
  ];
  const date = [
    { id: 1, name: "2021", unavailable: false },
    { id: 2, name: "2022", unavailable: false },
    { id: 3, name: "2023", unavailable: false },
    { id: 4, name: "2024", unavailable: true },
    { id: 5, name: "2024", unavailable: false },
  ];
  const status = [
    { id: 1, name: "True", unavailable: false },
    { id: 2, name: "False", unavailable: false },
  ];
  const tabs = [
    {
      id: 1,
      title: "Prospects",
      value: "18",
    },
    {
      id: 2,
      title: "Clients",
      value: "10",
    },
    {
      id: 1,
      title: "Archived",
      value: "0",

    },
  ];
  const [active, setActive] = useState(tabs[0]?.title);
  return (
    <div className="flex flex-col gap-3">
      <MainTableHeader />
      <div className="bg-white w-full">
        <div className="border-b">
          <div className="flex items-center justify-between px-[24px] h-[72px]">
            <div className="flex items-center gap-[20px]">
              <div>
                <div className="flex items-center justify-between px-[8px] py-[6px] border rounded-[4px]">
                  <div className="flex items-center gap-[12px]">
                    <img src={Search} alt="threeDot" />
                    <input
                      type="text"
                      placeholder="Search Particular"
                      className="focus:outline-none text-[14px] leading-[20px]"
                    />
                  </div>
                </div>
              </div>
              <Dropdown icon={Filter} people={people} />
              <Dropdown icon={Calander} people={date} />
              <Dropdown icon={Tick} people={status} />
            </div>
            <div className="flex items-center gap-[20px]">
              <div className="flex items-center gap-[6px] cursor-pointer">
                <img src={Filter} alt="filter" />
                <p className="text-[14px] leading-[17px] text-[#667085]">
                  Filter
                </p>
              </div>
              <img src={Line} alt="line" />
              <div className="flex items-center gap-[6px] cursor-pointer">
                <img src={DualArrow} alt="filter" />
                <p className="text-[14px] leading-[17px] text-[#667085]">
                  Sort
                </p>
              </div>
              <img src={Line} alt="line" />
              <div className="flex items-center gap-[6px] cursor-pointer">
                <img src={Filter} alt="filter" />
                <p className="text-[14px] leading-[17px] text-[#667085]">
                  Saved Filter
                </p>
              </div>
              <button className="text-[15px] font-medium leading-[20px] text-[#4786E7] ml-[14px] ">
                Clear
              </button>
            </div>
          </div>
        </div>
        <div className="py-[12px]">
          <div className="flex items-center justify-between px-[24px] ">
            <button className="flex items-center gap-[6px] bg-[#7474C9] text-[white] p-[12px] rounded-[4px]">
              New Client <img src={WhiteArrow} alt="down arrow" />
            </button>
            <div className="flex items-center">
              {tabs?.map((details: any) => (
                <div
                  key={details?.id}
                  className={`px-[12px] py-[7px] cursor-pointer ${active === details?.title
                    ? "border-b-[2px] border-[#4AC86E]"
                    : ""
                    }`}
                  onClick={() => setActive(details?.title)}
                >
                  <p className="text-[14px] leading-[17px] text-[#344054]">
                    {details?.title}({details?.value})
                  </p>
                </div>
              ))}
            </div>
          </div>
        </div>
        <Table />
      </div>
    </div>
  );
};

export default MainTable;
