import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import Navbar from "./components/Navbar";
import Sidebar from "./components/Sidebar";
import MainTable from "./components/MainTable";

function App() {
  const [count, setCount] = useState(0);

  return (
    <div className="flex flex-col bg-[#ededf3] gap-2 h-[100vh]">
      <Navbar />
      <div className="flex gap-2 h-[90%]">
        <div className="w-[20%] h-full">
          <Sidebar />
        </div>
        <div className="w-[80%] h-full">
          <MainTable />
        </div>
      </div>
    </div>
  );
}

export default App;
